

def eingabe_der_zahl():
    #der Bediener gibt eine Zahl ein
    eingabe = input("Bitte gebe eine natürliche Zahl  ein.")
    return int(eingabe)

def rechnen(eingegebene_zahl):
    # teilt durch alle natürlichen Zahlen bis zur eingegebenen Zahl
    for teiler in range(2, eingegebene_zahl):
        rest = eingegebene_zahl % teiler
       # print(f"Wenn man {eingegebene_zahl} durch {teiler} teilt, bleibt ein Rest von {rest}.")
        if (rest == 0):
            print(f"{eingegebene_zahl} ist durch {teiler} teilbar.")
            #Kein Rest, daher haben wir einen Teiler gefunden!
            return False

    return True

def ausgabe (benutzerzahl, ist_eine_primzahl):
    if (ist_eine_primzahl):
        print (f"Die Zahl {benutzerzahl} ist eine Primzahl")
    else:
        print (f"Die Zahl {benutzerzahl} ist keine Primzahl")

eingegebene_zahl = eingabe_der_zahl()
ist_eine_primzahl = rechnen(eingegebene_zahl)
ausgabe(eingegebene_zahl, ist_eine_primzahl)